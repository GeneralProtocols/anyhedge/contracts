import { Artifact } from 'cashscript'

declare interface AnyHedgeArtifacts
{
	'AnyHedge v0.12': Artifact,
}

declare interface UnsupportedAnyHedgeArtifacts
{
	'AnyHedge v0.11': Artifact,
	'AnyHedge v0.10': Artifact,
	'AnyHedge v0.9': Artifact,
}

declare const _exports:
{
	AnyHedgeArtifacts: AnyHedgeArtifacts,
	UnsupportedAnyHedgeArtifacts: UnsupportedAnyHedgeArtifacts,
};

export = _exports;
