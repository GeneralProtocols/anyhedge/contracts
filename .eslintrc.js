module.exports =
{
	extends: [ '@generalprotocols/eslint-config' ],
	parserOptions:
	{
		project: './tsconfig.json',
		ecmaVersion: 2018,
		sourceType: 'module',
	},
	rules:
	{
		camelcase: 'off',
		'import/extensions': 'off',
	},
};
