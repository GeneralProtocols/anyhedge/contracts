; Compiled on 2020-10-30
; Opcode count: 189 of 201
; Bytesize: (336~357) of 361 (520 - 159)
<maturityHeight> <earliestLiquidationHeight>            ; 2 * (3~4) + 2 * 1 bytes
<highLiquidationPrice> <lowLiquidationPrice>            ; 2 * (1~4) + 2 * 1 bytes
<lowTruncatedZeroes> <highLowDeltaTruncatedZeroes>      ; (0~4) + (0~3) + 2 * 1 bytes
<hedgeUnitsXSatsPerBch_HighTrunc> <payoutSats_LowTrunc> ; 2 * (1~4) + 2 * 1 bytes
<payoutDataHash> <mutualRedemptionDataHash>             ; 2 * 20 + 2 * 1 bytes
; Mutual Redeem
OP_10 OP_PICK OP_0 OP_NUMEQUAL OP_IF
    ; Verify data hash
    OP_11 OP_PICK OP_13 OP_PICK OP_CAT OP_HASH160 OP_EQUALVERIFY
    ; Verify hedge sig
    OP_12 OP_ROLL OP_11 OP_ROLL OP_CHECKSIGVERIFY
    ; Verify long sig
    OP_11 OP_ROLL OP_11 OP_ROLL OP_CHECKSIGVERIFY
    ; Cleanup
    OP_2DROP OP_2DROP OP_2DROP OP_2DROP OP_2DROP OP_1
; Payout
OP_ELSE OP_10 OP_ROLL OP_1 OP_NUMEQUALVERIFY
    ; Decode preimage
    ; tx.hashPrevouts
    OP_10 OP_PICK OP_4 OP_SPLIT OP_NIP 20 OP_SPLIT
    ; tx.outpoint
    20 OP_SPLIT OP_NIP 24 OP_SPLIT
    ; tx.hashOutputs
    OP_SIZE 28 OP_SUB OP_SPLIT OP_NIP 20 OP_SPLIT OP_DROP
    ; Verify there's only a single input
    OP_ROT OP_ROT OP_HASH256 OP_EQUALVERIFY
    ; Verify data hash
    OP_ROT OP_12 OP_PICK OP_14 OP_PICK OP_CAT OP_15 OP_PICK OP_CAT OP_HASH160 OP_EQUALVERIFY

    ; Verify oracle sig
    11 OP_ROLL 11 OP_PICK OP_15 OP_ROLL OP_CHECKDATASIGVERIFY
    ; Verify preimage
    OP_13 OP_ROLL OP_14 OP_ROLL OP_2DUP OP_SWAP OP_SIZE OP_1SUB OP_SPLIT OP_DROP
    OP_14 OP_ROLL OP_SHA256 OP_ROT OP_CHECKDATASIGVERIFY OP_CHECKSIGVERIFY
    ; Decode oracle message
    OP_12 OP_PICK OP_4 OP_SPLIT OP_NIP
    OP_DUP 24 OP_SPLIT OP_NIP
    OP_2 OP_SPLIT OP_DROP OP_BIN2NUM
    OP_14 OP_ROLL OP_4 OP_SPLIT OP_DROP OP_BIN2NUM
    OP_ROT OP_4 OP_SPLIT OP_DROP OP_BIN2NUM
    ; Check oracle price
    OP_OVER OP_0 OP_GREATERTHAN OP_VERIFY
    ; Clamp price
    OP_SWAP OP_10 OP_PICK OP_MIN OP_9 OP_PICK OP_MAX
    ; Check oracle height
    OP_OVER ff64cd1d OP_LESSTHAN OP_VERIFY
    ; Check locktime
    OP_OVER OP_CHECKLOCKTIMEVERIFY OP_DROP
    ; Check oracleHeight in bounds
    OP_OVER OP_12 OP_ROLL OP_13 OP_PICK OP_1ADD OP_WITHIN OP_VERIFY
    ; If: liquidation
    OP_SWAP OP_11 OP_ROLL OP_LESSTHAN OP_IF
        ; Check oracle price out of bounds
        OP_DUP OP_9 OP_PICK OP_1ADD OP_11 OP_PICK OP_WITHIN OP_NOT OP_VERIFY
    ; Else: maturation
    OP_ELSE
        ; Check exactly at maturity height
        OP_OVER OP_1 OP_NUMEQUALVERIFY
    OP_ENDIF
    ; TRUNCATION MATH
    ; DIV value high -> low truncation
    OP_4 OP_PICK OP_OVER OP_DIV
    OP_7 OP_PICK OP_SWAP OP_4 OP_NUM2BIN OP_CAT OP_BIN2NUM
    ; MOD value high -> truncation
    OP_5 OP_ROLL OP_2 OP_PICK OP_MOD
    ; Calculate and apply mod extension
    OP_7 OP_ROLL OP_SIZE OP_NIP
    OP_4 OP_2 OP_PICK OP_SIZE OP_NIP OP_SUB OP_OVER OP_MIN
    OP_0 OP_OVER OP_NUM2BIN
    OP_3 OP_ROLL OP_4 OP_NUM2BIN OP_CAT OP_BIN2NUM
    ; Calculate and apply price truncation
    OP_ROT OP_ROT OP_SUB
    OP_3 OP_ROLL OP_SWAP OP_SPLIT OP_NIP OP_BIN2NUM
    ; Calculate hedge sats: Divide extended MOD value by truncated price and clamp
    OP_DIV OP_ADD OP_4 OP_PICK OP_MIN
    ; Calculate long sats
    OP_4 OP_ROLL OP_OVER OP_SUB
    ; Verify hash outputs
    2202 OP_8 OP_NUM2BIN
    OP_8 OP_7 OP_PICK OP_SIZE OP_NIP OP_SUB
    OP_7 OP_PICK OP_4 OP_ROLL OP_2 OP_PICK OP_NUM2BIN OP_CAT OP_2 OP_PICK OP_OR
    OP_10 OP_ROLL OP_CAT
    OP_7 OP_ROLL OP_4 OP_ROLL OP_3 OP_ROLL OP_NUM2BIN OP_CAT OP_ROT OP_OR
    OP_7 OP_ROLL OP_CAT
    OP_3 OP_ROLL OP_ROT OP_ROT OP_CAT OP_HASH256 OP_EQUALVERIFY
    ; Cleanup
    OP_2DROP OP_2DROP OP_1
OP_ENDIF
