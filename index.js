const anyhedge_v0_9 = require('./contracts/v0.9/artifact');
const anyhedge_v0_10 = require('./contracts/v0.10/artifact');
const anyhedge_v0_11 = require('./contracts/v0.11/artifact');
const anyhedge_v0_12 = require('./contracts/v0.12/artifact');

const AnyHedgeArtifacts =
{
	'AnyHedge v0.12': anyhedge_v0_12,
};

const UnsupportedAnyHedgeArtifacts =
{
	'AnyHedge v0.11': anyhedge_v0_11,
	'AnyHedge v0.10': anyhedge_v0_10,
	'AnyHedge v0.9': anyhedge_v0_9,
};

module.exports =
{
	AnyHedgeArtifacts,
	UnsupportedAnyHedgeArtifacts,
};
