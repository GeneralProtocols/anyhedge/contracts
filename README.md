# AnyHedge Smart Contracts
This repository contains the versioned smart contract source code, bytecode and artifact JSON files. The source code and bytecode are mainly stored for reference, while the artifact JSON objects are exported so they can be used inside the AnyHedge Library package.

## Repository Structure
Contracts of a specific version can be found under `contracts/vX.Y/`. These versioned directories contain a `contract.cash`, `bytecode.asm` and `artifact.json` file. In the root directory there is an `index.js` and an `index.d.ts` file. These files export the artifacts for the different versions.

## Definition of Dust Since v0.12.1
See derivation: https://bitcoincashresearch.org/t/friday-night-challenge-worst-case-dust/1181/2
